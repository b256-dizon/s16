let num1 = 25;
	let num2 = 5;
	console.log("The result of num1 + num2 should be 30.");
	console.log("Actual Result:");
	console.log(num1 + num2);

	let num3 = 156;
	let num4 = 44;
	console.log("The result of num3 + num4 should be 200.");
	console.log("Actual Result:");
	console.log(num3 + num4);

	let num5 = 17;
	let num6 = 10;
	console.log("The result of num5 - num6 should be 7.");
	console.log("Actual Result:");
	console.log(num5-num6);


	let minutesHour = 60;
	let hoursDay = 24;
	let daysWeek = 7;
	let weeksMonth = 4;
	let monthsYear = 12;
	let daysYear = 365;

	let day = minutesHour * hoursDay;
	let month = day * daysYear;
	console.log("There are " + month + " in a year.")

	let tempCelsius = 132;
	const c = 1.8;
	let farenheit = (c*132) +32 ;
	console.log ("132 degrees Celsius when converted to Farenheit is " + farenheit);

	let num7 = 165;
	let modulo = num7 % 8 ;
	console.log("The remainder of 165 divided by 8 is:" + modulo);

	console.log("Is num7 divisible by 8?");
	console.log('num7' === '8' );


	let num8 = 348;
	let mod = num8 % 4;
	console.log("The remainder of 348 divided by 4 is:" + mod);
	console.log("Is num8 divisible by 4?");
	console.log('num8' !== mod);